package tribo8;

import static robocode.util.Utils.normalRelativeAngleDegrees;
import java.awt.Color;
import java.util.LinkedList;
import java.util.List;

import robocode.AdvancedRobot;
import robocode.HitByBulletEvent;
import robocode.ScannedRobotEvent;
import robocode.StatusEvent;
import robocode.TurnCompleteCondition;
import robocode.WinEvent;
import robocode.RobotStatus;

import ibmb.tools.CircularIntercept;
import ibmb.tools.Enemy;
import ibmb.tools.GravPoint;

/**
 * Odesseiron - a robot by Italo Barretto
 */

public class Gravity8 extends AdvancedRobot{
	
	private RobotStatus robotStatus;
	private int roundsWithoutTarget = 0;
	private double radarTurnAmount;
	private double previousHeading;
	private Enemy target;					
	private final double PI = Math.PI;
	private int randomPointCount = 0;
	private boolean onHit = false;
	
	public void run() {
		
		setBodyColor(Color.black);
		setGunColor(Color.black);
		setRadarColor(Color.orange);
		setBulletColor(Color.red);
		setScanColor(Color.blue);
		
		setAdjustGunForRobotTurn(true);
		setAdjustRadarForGunTurn(true);
		setAdjustRadarForRobotTurn(true);
		
		target = new Enemy();
		target.setDistance(100000);
		radarTurnAmount = 360;
		
		
		while(true) {
			
			//Adquirir alvo
			roundsWithoutTarget++;
			if (roundsWithoutTarget > 2) {
				target.setOnSight(false);
				radarTurnAmount = 360;
			}
			if(!target.isOnSight()) {
				setTurnGunRight(radarTurnAmount);
				setTurnRadarRight(radarTurnAmount);
			}
			
			antiGravMove();
			
			execute();
		}
	}
	
	public void onStatus(StatusEvent e) {
        this.robotStatus = e.getStatus();
    }   

	public void onScannedRobot(ScannedRobotEvent e) {
		target.setOnSight(true);
		
        radarTurnAmount =  normalRelativeAngleDegrees(getHeading() + e.getBearing() - getRadarHeading());
		setTurnRadarRight(2.5 * radarTurnAmount); //Radar lock
		
		int bulletPower = 3;
		
		if (getEnergy() > 70) {
			
			bulletPower = 3;
	 
		} else if (getEnergy() > 39) {
	 
			bulletPower = 2;
	 
		} else {
			bulletPower = 1;
		}
		
		//Informa��es sobre o alvo
		double enemyAngle = (getHeadingRadians()+e.getBearingRadians())%(2*PI);
		target.setName(e.getName());
		target.setX(getX()+Math.sin(enemyAngle)*e.getDistance());
		target.setY(getY()+Math.cos(enemyAngle)*e.getDistance());
		target.setHeading(e.getHeading());
		target.setSpeed(e.getVelocity());
		target.setDistance(e.getDistance());
		
		//Estima a localiza��o futura do inimigo 
		CircularIntercept intercept = new CircularIntercept();
		double angularVelocity_deg_per_sec = target.getHeading() - previousHeading;
		previousHeading = target.getHeading();
		intercept.calculate(getX(),getY(),target.getX(),target.getY(),target.getHeading(),target.getSpeed(),bulletPower,angularVelocity_deg_per_sec);
		double turnAngle = normalRelativeAngleDegrees(intercept.bulletHeading_deg - robotStatus.getGunHeading());
		setTurnGunRight(turnAngle);
		
		if (Math.abs(turnAngle) <= intercept.angleThreshold) {
			//Verifica se ponto de impacto previsto encontra-se dentro da arena
			if (
				(intercept.impactPoint.getX() > 0) &&
				(intercept.impactPoint.getX() < getBattleFieldWidth()) &&
				(intercept.impactPoint.getY() > 0) &&
				(intercept.impactPoint.getY() < getBattleFieldHeight())
			) {
				//Verifica se o alvo encontra-se em vis�o a mais de um turno e se posssui energia suficiente para atirar
				if(roundsWithoutTarget == 0 && getEnergy() > 3.0) {
					fire(bulletPower);
				}
			 }
		}
		
		roundsWithoutTarget = 0;
	
		antiGravMove();
		
		scan();
	}
		
	public void onHitByBullet(HitByBulletEvent e) {
		turnRight(45);
		ahead(200);
	}

	public void onWin(WinEvent e) {
		for (int i = 0; i < 50; i++) {
			setTurnRight(30);
			waitFor(new TurnCompleteCondition(this));
			setTurnLeft(30);
			waitFor(new TurnCompleteCondition(this));
			setTurnRadarRight(360);
			
			execute();
		}
	}
	
	//L�gica da Movimenta��o Antigravidade
	void antiGravMove() {
   		double xForce = 0;
	    double yForce = 0;
	    double force;
	    double averageForce = 0;
	    double ang;
	    GravPoint p;
	    GravPoint randomP;
	    List<GravPoint> randomPoints = new LinkedList<GravPoint>();
	    List<GravPoint> robots = new LinkedList<GravPoint>();
	    
		// Seta o GravPoint do inimigo    	
		p = new GravPoint();
		p.setX(target.getX());
		p.setY(target.getY());
		p.setPower(-1000);
		robots.add(p);
        force = p.getPower()/Math.pow(getRange(getX(),getY(),p.getX(),p.getY()),2);
        ang = normaliseHeading(PI/2 - Math.atan2(getY() - p.getY(), getX() - p.getX()));
        
        //Componente de For�a do inimigo
        xForce += Math.sin(ang) * force;
        yForce += Math.cos(ang) * force;
		
        //Cria quatro pontos de atra��o ou repuls�o nos cantos da arena
		randomPointCount++;
		if(randomPointCount > 5) {
			randomPointCount = 0;
			
			while(randomPoints.size() < 4) {
				randomP = new GravPoint();
				randomPoints.add(randomP);
				switch (randomPoints.size()) {
					case 1:
						randomP.setX(getBattleFieldWidth() - (Math.random() * 100 + 50));
						randomP.setY(getBattleFieldHeight() - (Math.random() * 100 + 50));
						break;
					case 2:
						randomP.setX(getBattleFieldWidth() - (Math.random() * 100 + 50));
						randomP.setY(Math.random() * 100 + 50);
						break;
					case 3:
						randomP.setX(Math.random() * 100 + 50);
						randomP.setY(getBattleFieldHeight() - (Math.random() * 100 + 50));
						break;
					case 4:
						randomP.setX(Math.random() * 100 + 50);
						randomP.setY(Math.random() * 100 + 50);
						break;
				}
			}
			
			p = new GravPoint();
			p.setX(getX());
			p.setY(getY());
			p.setPower(-1000);
			robots.add(p);
			
			for(GravPoint gp : randomPoints) {
				force = 0;
				for(GravPoint robot : robots) {
					force += robot.getPower()/Math.pow(getRange(gp.getX(),gp.getY(),robot.getX(),robot.getY()),1.5);
				}
				gp.setForce(force);
				averageForce += gp.getForce();
			}
			
			averageForce /= randomPoints.size();
			
			for(GravPoint gp : randomPoints) {
				
				if(gp.getForce() < averageForce) {
					gp.setPower(-5000);
				}else {
					gp.setPower(5000);
				}
				
				force = gp.getPower()/Math.pow(getRange(getX(),getY(),gp.getX(),gp.getY()),1.5);
			    ang = normaliseHeading(Math.PI/2 - Math.atan2(getY() - gp.getY(), getX() - gp.getX())); 
			    xForce += Math.sin(ang) * force;
			    yForce += Math.cos(ang) * force;
			}
			
		}
	   
		//Evitar colis�o com paredes
	    xForce += 5000/Math.pow(getRange(getX(), getY(), getBattleFieldWidth(), getY()), 3);
	    xForce -= 5000/Math.pow(getRange(getX(), getY(), 0, getY()), 3);
	    yForce += 5000/Math.pow(getRange(getX(), getY(), getX(), getBattleFieldHeight()), 3);
	    yForce -= 5000/Math.pow(getRange(getX(), getY(), getX(), 0), 3);
	    
	    goTo(getX() - xForce, getY() - yForce);
	}
	
	//Mover-se com base na dire��o das for�as resultantes em x e y
	void goTo(double x, double y) {
	    double dist = 20; 
	    double angle = Math.toDegrees(absHeading(getX(),getY(),x,y));
	    double r = turnTo(angle);
	    setAhead(dist * r);
	}

	//Gira para o menor �ngulo poss�vel de forma a alcan�ar a dire��o desejada, e depois retorna a dire��o
	int turnTo(double angle) {
	    double ang;
    	int dir;
	    ang = normaliseHeading(getHeading() - angle);
	    if (ang > 90) {
	        ang -= 180;
	        dir = -1;
	    }
	    else if (ang < -90) {
	        ang += 180;
	        dir = -1;
	    }
	    else {
	        dir = 1;
	    }
	    setTurnLeft(ang);
	    return dir; // Para frente = 1, Para tr�s = -1
	}
	
	//Se a dire��o desejada n�o estiver entre 0 e 2*PI, ela � alterada para se encaixar nesta faixa
	double normaliseHeading(double ang) {
		if (ang > PI)
			ang -= 2*PI;
		if (ang < -PI)
			ang += 2*PI;
		return ang;
	}
	
	//Retorna dist�ncia entre dois pontos
	public double getRange(double x1, double y1, double x2, double y2)
	{
		double xo = x2-x1;
		double yo = y2-y1;
		double h = Math.sqrt( xo*xo + yo*yo );
		return h;	
	}
	
	//Retorna dire��o absoluta entre dois pontos
	public double absHeading(double x1, double y1, double x2, double y2)
	{
		double xo = x2 - x1;
		double yo = y2 - y1;
		double h = getRange( x1,y1, x2,y2 );
		
		if( xo > 0 && yo > 0 ){
			return Math.asin( xo / h );
		}
		if( xo > 0 && yo < 0 ){
			return PI - Math.asin( xo / h );
		}
		if( xo < 0 && yo < 0 ){
			return PI + Math.asin( -xo / h );
		}
		if( xo < 0 && yo > 0 ){
			return 2.0*PI - Math.asin( -xo / h );
		}
		return 0;
	}
	
}