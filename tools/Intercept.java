package tribo8.tools;

import java.awt.geom.Point2D;

public class Intercept {
	 public Point2D.Double impactPoint = new Point2D.Double(0,0);
	 public double bulletHeading_deg;
	 protected Point2D.Double bulletStartingPoint = new Point2D.Double();
	 protected Point2D.Double targetStartingPoint = new Point2D.Double();
	 public double targetHeading;
	 public double targetVelocity;
	 public double bulletPower;
	 public double angleThreshold;
	 public double distance;
	 protected double impactTime;
	 protected double angularVelocity_rad_per_sec;
	 
	 public void calculate (double initialPositionX, double initialPositionY, double targetPositionX,
			 				double targetPositionY, double tHeading, double tVelocity, double bPower,
			 				double angularVelocity_deg_per_sec)
	{
		angularVelocity_rad_per_sec = Math.toRadians(angularVelocity_deg_per_sec);
		bulletStartingPoint.setLocation(initialPositionX, initialPositionY);
		targetStartingPoint.setLocation(targetPositionX, targetPositionY);
		targetHeading = tHeading;
		targetVelocity = tVelocity;
		bulletPower = bPower;
		
		double distanceX,distanceY;
		
		// Suposições iniciais 10 e 20 ticks
		impactTime = getImpactTime(10, 20, 0.01);
		impactPoint = getEstimatedPosition(impactTime);
		distanceX = (impactPoint.getX() - bulletStartingPoint.getX());
		distanceY = (impactPoint.getY() - bulletStartingPoint.getY());
		distance = Math.sqrt(distanceX*distanceX + distanceY*distanceY);
		bulletHeading_deg = Math.toDegrees(Math.atan2(distanceX,distanceY));
		angleThreshold = Math.toDegrees(Math.atan(10/distance));
	}
	 
	protected Point2D.Double getEstimatedPosition(double time) {
		double x = targetStartingPoint.getX() + targetVelocity * time * Math.sin(Math.toRadians(targetHeading));
		double y = targetStartingPoint.getY() + targetVelocity * time * Math.cos(Math.toRadians(targetHeading));
		
		return new Point2D.Double(x,y);
	}
	
	private double accuracyTest(double time) {
		double bulletSpeed = 20-3*bulletPower;
		Point2D.Double targetPosition = getEstimatedPosition(time);
		double distanceX = (targetPosition.getX() - bulletStartingPoint.getX());
		double distanceY = (targetPosition.getY() - bulletStartingPoint.getY());
		
		return Math.sqrt(distanceX*distanceX + distanceY*distanceY) - bulletSpeed * time;
	}
	
	private double getImpactTime(double t0, double t1, double accuracy) {
		double X = t1;
		double lastX = t0;
		int count = 0;
		double lastfX = accuracyTest(lastX);
		
		while ((Math.abs(X - lastX) >= accuracy) && (count < 15)) {
			count++;
			double fX = accuracyTest(X);
			if ((fX-lastfX) == 0.0) break;
			double nextX = X - fX*(X-lastX)/(fX-lastfX);
			lastX = X;
			X = nextX;
			lastfX = fX;
		}
		
		return X;
	}
}
