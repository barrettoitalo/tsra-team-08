package tribo8.tools;

public class GravPoint {
	private double x,y,power,force;

    public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}
    
	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getPower() {
		return power;
	}

	public void setPower(double power) {
		this.power = power;
	}

	public double getForce() {
		return force;
	}

	public void setForce(double force) {
		this.force = force;
	}
}
